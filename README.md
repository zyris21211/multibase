# Multibase
## Run options
### Test
`mvn test` - it will start 3 test container (PostgreSQL, MySQL and Oracle). Test should check DB 
### With local or remote database
To start application localy we should
- add proper url and mappings into configurations/application.yml
  - optional: change server.port if provided one is occupied
- run:\
`mvn clean package -Dmaven.test.skip=true && java -cp target/multibase-1.0-SNAPSHOT.jar:configurations/ org.springframework.boot.loader.launch.JarLauncher`
  - To run tests we should remove `-Dmaven.test.skip=true` from `mvn` command
- check application using [possible options](#Check)

### With local docker compose and PG database
To start application we should run command: `docker compose up`\
It will run PostgreSQL and MultiBase app.

To check how it works we can use [possible options](#Check)

## Check
To check running application we can use:
- use auto-generate ui: http://localhost:42100/swagger-ui/index.html
- `curl http://localhost:42100/v1/users`