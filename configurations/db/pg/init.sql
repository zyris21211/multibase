CREATE TABLE users (
    user_id character varying(255) NOT NULL UNIQUE,
    login character varying(255) NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL
);

INSERT INTO users (user_id, login, first_name, last_name) VALUES
('pg_id', 'pg_username', 'pg_name', 'pg_surname');