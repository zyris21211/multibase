package space.zyrisdev.multibase.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import space.zyrisdev.multibase.domain.User;
import space.zyrisdev.multibase.service.MultiBaseService;

import java.util.List;

@RestController
@RequestMapping("/v1")
@RequiredArgsConstructor
public class MultiBaseController {
    private final MultiBaseService service;

    @Operation(summary = "Get all user from all possible databases")
    @GetMapping("/users")
    public List<User> fetchUsers() {
        return service.fetch();
    }
}
