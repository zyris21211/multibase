package space.zyrisdev.multibase.dao;

public class RequestProvider {
    private final String conditionalSql;


    public RequestProvider(String tableName) {
        this.conditionalSql = "SELECT * FROM %s".formatted(tableName);
    }


    public String getSql() {
        return conditionalSql;
    }

}
