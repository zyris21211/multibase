package space.zyrisdev.multibase.dao;

import lombok.RequiredArgsConstructor;
import space.zyrisdev.multibase.domain.User;
import space.zyrisdev.multibase.exception.QueryException;
import space.zyrisdev.multibase.service.mapping.Mapper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class UserRepository {
    private final String name;
    private final Connection connection;
    private final RequestProvider requestProvider;
    private final Mapper mapper;

    public List<User> fetch() {
        try (Statement stmt = connection.createStatement()) {
            try (ResultSet resultSet = stmt.executeQuery(requestProvider.getSql())) {
                List<User> users = new ArrayList<>();
                while (resultSet.next()) {
                    users.add(mapper.map(resultSet));
                }
                return users;
            }
        } catch (SQLException e) {
            throw new QueryException(e);
        }
    }
}
