package space.zyrisdev.multibase.exception;

public class ConnectionEstablishingException extends RuntimeException {
    public ConnectionEstablishingException(String message, Throwable cause) {
        super(message, cause);
    }

}
