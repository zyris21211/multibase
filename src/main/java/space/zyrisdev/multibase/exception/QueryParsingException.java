package space.zyrisdev.multibase.exception;

public class QueryParsingException extends RuntimeException {
    public QueryParsingException(Throwable cause) {
        super(cause);
    }
}
