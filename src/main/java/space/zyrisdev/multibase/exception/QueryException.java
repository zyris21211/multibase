package space.zyrisdev.multibase.exception;

public class QueryException extends RuntimeException {
    public QueryException(Throwable cause) {
        super(cause);
    }
}
