package space.zyrisdev.multibase.domain;

public record User(String id, String username, String name, String surname) {
}
