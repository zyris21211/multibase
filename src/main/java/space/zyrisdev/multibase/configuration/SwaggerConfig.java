package space.zyrisdev.multibase.configuration;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI springOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("MultiBase").description("Service that can work with several DB simultaneously").version("1.0-SNAPSHOT"))

                .externalDocs(new ExternalDocumentation().description("Task description")
                        .url("https://drive.google.com/file/d/1JEY9x5C9Yjmw4cHYCiu7SGKvxlkCLvtJ/view?usp=sharing")
                );
    }
}