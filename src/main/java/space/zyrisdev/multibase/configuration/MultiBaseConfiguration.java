package space.zyrisdev.multibase.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import space.zyrisdev.multibase.configuration.properties.MultiBaseProperties;
import space.zyrisdev.multibase.dao.RequestProvider;
import space.zyrisdev.multibase.dao.UserRepository;
import space.zyrisdev.multibase.service.mapping.ConfigurableMappingFactory;
import space.zyrisdev.multibase.service.mapping.Mapper;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@Configuration
@EnableConfigurationProperties(MultiBaseProperties.class)
public class MultiBaseConfiguration {

    @Bean
    List<UserRepository> repositoriesConfiguration(MultiBaseProperties properties,
                                                   Map<String, Mapper> mappersMap,
                                                   Map<String, Connection> connectionMap,
                                                   Map<String, RequestProvider> requestProviderMap) {

        return properties.getDataSources().stream()
                .map(prop -> new UserRepository(
                                prop.getName(),
                                connectionMap.get(prop.getName()),
                                requestProviderMap.get(prop.getName()),
                                mappersMap.get(prop.getName())
                        )
                )
                .toList();
    }

    @Bean
    Map<String, RequestProvider> requestProviderMap(MultiBaseProperties properties) {
        return properties.getDataSources().stream()
                .collect(
                        toMap(
                                MultiBaseProperties.DataSourcesProperties::getName,
                                prop -> new RequestProvider(prop.getTable())
                        )
                );
    }

    @Bean
    Map<String, Mapper> mappersMap(MultiBaseProperties properties, ConfigurableMappingFactory mappingFactory) {
        return properties.getDataSources().stream()
                .collect(
                        toMap(
                                MultiBaseProperties.DataSourcesProperties::getName,
                                prop -> mappingFactory.construct(prop.getMapping())
                        )
                );
    }

    @Bean
    Map<String, Connection> connectionMap(MultiBaseProperties properties) {
        return properties.getDataSources().stream()
                .collect(
                        toMap(
                                MultiBaseProperties.DataSourcesProperties::getName,
                                prop -> ConnectionProvider.builder()
                                        .strategy(prop.getStrategy())
                                        .url(prop.getUrl())
                                        .user(prop.getUser())
                                        .password(prop.getPassword())
                                        .build()
                                        .getConnection()
                        )
                );
    }

}
