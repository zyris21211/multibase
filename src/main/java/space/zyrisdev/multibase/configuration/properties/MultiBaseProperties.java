package space.zyrisdev.multibase.configuration.properties;

import jakarta.validation.Valid;
import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;
import space.zyrisdev.multibase.service.mapping.MappingSettings;

import java.util.ArrayList;
import java.util.List;

@Data
@ConfigurationProperties
@Validated
public class MultiBaseProperties {

    @Valid
    private List<DataSourcesProperties> dataSources = new ArrayList<>();

    @Data
    public static class DataSourcesProperties {
        @NotNull
        private String name;
        @NotNull
        private String strategy;
        @NotNull
        private String url;
        @NotNull
        private String table;
        @NotNull
        private String user;
        @NotNull
        private String password;
        @NotNull
        private Mapping mapping;

        public Strategy getStrategy() {
            return Strategy.ofName(strategy);
        }

        @AssertTrue(message = "dataSources.strategy is not valid!, use one of: [postgres, oracle, mysql]")
        boolean isValidStrategy() {
            return Strategy.ofName(strategy) != null;
        }
    }

    @Data
    public static class Mapping implements MappingSettings {
        @NotNull
        private String id;
        @NotNull
        private String username;
        @NotNull
        private String name;
        @NotNull
        private String surname;
    }

}
