package space.zyrisdev.multibase.configuration.properties;

import java.util.Arrays;

public enum Strategy {
    POSTGRES("postgres"), ORACLE("oracle"), MYSQL("mysql");

    private final String name;

    Strategy(String name) {
        this.name = name;
    }

    static Strategy ofName(String name) {
        return Arrays.stream(Strategy.values())
                .filter(s -> s.name.equals(name))
                .findAny()
                .orElse(null);
    }
}
