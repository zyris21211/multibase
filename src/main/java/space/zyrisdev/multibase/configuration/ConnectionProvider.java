package space.zyrisdev.multibase.configuration;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import space.zyrisdev.multibase.configuration.properties.Strategy;
import space.zyrisdev.multibase.exception.ConnectionEstablishingException;

import java.sql.Connection;
import java.sql.DriverManager;

@RequiredArgsConstructor
@Builder
@Slf4j
public class ConnectionProvider {

    private final Strategy strategy;
    private final String url;
    private final String user;
    private final String password;

    public Connection getConnection() {
        try {
            log.info(
                    "strategy: {}, url: {}, user: {}, password: {}", strategy, url, user, password
            );
            return DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            throw new ConnectionEstablishingException("Exception during connection establishing", e);
        }
    }
}
