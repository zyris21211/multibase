package space.zyrisdev.multibase.service.mapping;

import space.zyrisdev.multibase.domain.User;

import java.sql.ResultSet;

@FunctionalInterface
public interface Mapper {
    User map(ResultSet resultSet);
}
