package space.zyrisdev.multibase.service.mapping;

public interface MappingSettings {
    String getId();

    String getUsername();

    String getName();

    String getSurname();
}
