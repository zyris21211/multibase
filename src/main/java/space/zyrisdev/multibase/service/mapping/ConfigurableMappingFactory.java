package space.zyrisdev.multibase.service.mapping;

import org.springframework.stereotype.Component;
import space.zyrisdev.multibase.domain.User;
import space.zyrisdev.multibase.exception.QueryParsingException;

import java.sql.SQLException;

@Component
public class ConfigurableMappingFactory {
    public Mapper construct(MappingSettings settings) {
        return (result) -> {
            try {
                return new User(
                        result.getString(settings.getId()),
                        result.getString(settings.getUsername()),
                        result.getString(settings.getName()),
                        result.getString(settings.getSurname())
                );
            } catch (SQLException e) {
                throw new QueryParsingException(e);
            }
        };
    }
}
