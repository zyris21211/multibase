package space.zyrisdev.multibase.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import space.zyrisdev.multibase.dao.UserRepository;
import space.zyrisdev.multibase.domain.User;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MultiBaseService {
    private final List<UserRepository> userRepositoryList;

    public List<User> fetch() {
        return userRepositoryList.stream()
                .flatMap(s -> s.fetch().stream())
                .toList();
    }
}
