CREATE TABLE users_oracle (
    oracle_user_id VARCHAR2(255) NOT NULL,
    oracle_login VARCHAR2(255) NOT NULL,
    oracle_first_name VARCHAR2(255) NOT NULL,
    oracle_last_name VARCHAR2(255) NOT NULL,
    constraint users_pk PRIMARY KEY(oracle_user_id)
);

INSERT INTO users_oracle (oracle_user_id, oracle_login, oracle_first_name, oracle_last_name) VALUES
('oracle_id', 'oracle_username', 'oracle_name', 'oracle_surname');