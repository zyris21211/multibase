CREATE TABLE users_pg (
    pg_user_id character varying(255) NOT NULL UNIQUE,
    pg_login character varying(255) NOT NULL,
    pg_first_name character varying(255) NOT NULL,
    pg_last_name character varying(255) NOT NULL
);

INSERT INTO users_pg (pg_user_id, pg_login, pg_first_name, pg_last_name) VALUES
('pg_id', 'pg_username', 'pg_name', 'pg_surname');