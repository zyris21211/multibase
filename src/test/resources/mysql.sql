CREATE TABLE users_mysql (
    mysql_user_id varchar(255) NOT NULL,
    mysql_login varchar(255) NOT NULL,
    mysql_first_name varchar(255) NOT NULL,
    mysql_last_name varchar(255) NOT NULL
);

INSERT INTO users_mysql (mysql_user_id, mysql_login, mysql_first_name, mysql_last_name) VALUES
('mysql_id', 'mysql_username', 'mysql_name', 'mysql_surname');