package space.zyrisdev.multibase.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.oracle.OracleContainer;
import space.zyrisdev.multibase.configuration.ConnectionProvider;
import space.zyrisdev.multibase.configuration.properties.Strategy;
import space.zyrisdev.multibase.dao.RequestProvider;
import space.zyrisdev.multibase.dao.UserRepository;
import space.zyrisdev.multibase.domain.User;
import space.zyrisdev.multibase.exception.QueryParsingException;

import java.sql.SQLException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@EnableAutoConfiguration(exclude = {MongoAutoConfiguration.class, DataSourceAutoConfiguration.class})
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = MultiBaseServiceTest.MultiBaseConfiguration.class)
@Testcontainers
class MultiBaseServiceTest {
    @Autowired
    MultiBaseService service;

    @Container
    public static PostgreSQLContainer<?> postgreContainer = new PostgreSQLContainer<>("postgres:16.2-alpine3.19")
            .withDatabaseName("postgres")
            .withPassword("postgres")
            .withPassword("postgres")
            .withInitScript("pg.sql");

    @Container
    static OracleContainer oracleContainer =
            new OracleContainer("gvenzl/oracle-free:23-slim")
                    .withDatabaseName("oracle")
                    .withUsername("oracle")
                    .withPassword("oracle")
                    .withInitScript("oracle.sql");
    @Container
    static MySQLContainer mySQLContainer = new MySQLContainer<>("mysql:8.3")
            .withDatabaseName("mysql")
            .withUsername("mysql")
            .withPassword("mysql")
            .withInitScript("mysql.sql");

    @TestConfiguration
    public static class MultiBaseConfiguration {
        @Primary
        @Bean
        public List<UserRepository> repositoriesConfiguration() {
            return List.of(

                    new UserRepository("postgres",
                            new ConnectionProvider(
                                    Strategy.POSTGRES,
                                    postgreContainer.getJdbcUrl(),
                                    postgreContainer.getUsername(),
                                    postgreContainer.getPassword()
                            ).getConnection()
                            ,
                            new RequestProvider("users_pg"),
                            (result) -> {
                                try {
                                    return new User(
                                            result.getString("pg_user_id"),
                                            result.getString("pg_login"),
                                            result.getString("pg_first_name"),
                                            result.getString("pg_last_name")
                                    );
                                } catch (SQLException e) {
                                    throw new QueryParsingException(e);
                                }
                            }
                    ),

                    new UserRepository("oracle",
                            new ConnectionProvider(
                                    Strategy.ORACLE,
                                    oracleContainer.getJdbcUrl(),
                                    oracleContainer.getUsername(),
                                    oracleContainer.getPassword()
                            )
                                    .getConnection(),
                            new RequestProvider("users_oracle"),
                            (result) -> {
                                try {
                                    return new User(
                                            result.getString("oracle_user_id"),
                                            result.getString("oracle_login"),
                                            result.getString("oracle_first_name"),
                                            result.getString("oracle_last_name")
                                    );
                                } catch (SQLException e) {
                                    throw new QueryParsingException(e);
                                }
                            }
                    ),
                    new UserRepository("mysql",
                            new ConnectionProvider(
                                    Strategy.MYSQL,
                                    mySQLContainer.getJdbcUrl(),
                                    mySQLContainer.getUsername(),
                                    mySQLContainer.getPassword()
                            ).getConnection(),
                            new RequestProvider("users_mysql"),
                            (result) -> {
                                try {
                                    return new User(
                                            result.getString("mysql_user_id"),
                                            result.getString("mysql_login"),
                                            result.getString("mysql_first_name"),
                                            result.getString("mysql_last_name")
                                    );
                                } catch (SQLException e) {
                                    throw new QueryParsingException(e);
                                }
                            }
                    )
            );
        }
    }


    @Test
    void shouldGetCustomers() {
        List<User> provided = service.fetch();

        User expectedPg = new User("pg_id", "pg_username", "pg_name", "pg_surname");
        User expectedOracle = new User("oracle_id", "oracle_username", "oracle_name", "oracle_surname");
        User expectedMySQL = new User("mysql_id", "mysql_username", "mysql_name", "mysql_surname");

        assertThat(provided).containsExactlyInAnyOrder(expectedPg, expectedOracle, expectedMySQL);
    }
}