FROM maven:3.9.6-eclipse-temurin-21-alpine as builder
WORKDIR /app
COPY . /app/.
RUN mvn -f /app/pom.xml clean package -Dmaven.test.skip=true

FROM eclipse-temurin:21-jdk-alpine
WORKDIR /app
COPY --from=builder /app/target/*.jar /app/*.jar
COPY /configurations/application-pg.yml /app/application.yml
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/*.jar"]